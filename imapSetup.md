# This is FSMK Email Setup Guide for Thunderbird

This document should help you set up your email-client with the proper
settings so that you can use your fsmk.org email address.  We
recommend using Thunderbird as the email-client and thus for this demo
we will use Thunderbird.


# Follow the Steps below 
We have the steps shown below with proper images. Some parts are redacted to ensure privacy.

## Step 1
You should see the box while setting up a new email account in Thunderbird.

Since we have the email account is already created and we just need to configure Thunderbird we will select the first option: **Skip this and use my existing Account**

![step1](./assets/1.png)

## Step 2
After clicking that we should see a box like the one shown below. 

![step2](./assets/2.png)

In this page we will enter the name of the account for Thunderbird, the email address and the password. 

Then we will select **Continue**

## Step 3

![step3](./assets/3.png)
It will load for some time. During this time Thunderbird is talking with the server and figuring out some details on its own.

## Step 4

![step4](./assets/4.png)

In this step select IMAP. 

## Step 5
![step5](./assets/5.png)

This is the **most** crucial step. 

Make sure that the incoming and outgoing **usernames** are <email>.fsmk where <email> is the *local* part of the email address. 
eg. if your email address on fsmk.org is randomuser@fsmk.org then the IMAP username (both incoming and outgoing) is *randomuser.fsmk*

Make sure that the incoming and outgoing ports are correct too. 

Now select **DONE**

## Step 6
Select **Done** and you setup will be finished. 

![step6](./assets/6.png)



#### A lot of care was taken to prep this document. If you find errors/inaccuracies then let us know. :-)
